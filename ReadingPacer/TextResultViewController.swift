//
//  ResultViewController.swift
//  StepReading
//
//  Created by SHOKI TAKEDA on 11/3/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class TextResultViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, NADViewDelegate {
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var showCountSecond: UILabel!
    var timer:NSTimer!
    var startCount = true
    var counter:Double = 0
    var mainEnglishText:String?
    var adTimer:NSTimer!
    private var nadView: NADView!
    @IBOutlet weak var pauseLabel: UIButton!
    @IBAction func pauseButton(sender: UIButton) {
        timer.invalidate()
        startButton.hidden = false
        pauseLabel.hidden = true
    }
    @IBAction func startSentences(sender: UIButton) {
//        englishLabel.attributedText = globalEnglishLabel[0]
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
        startButton.hidden = true
        pauseLabel.hidden = false
        showCountSecond.text = ""
    }
    func wordCount(s: String) -> Array<String> {
        let separators = NSCharacterSet(charactersInString: " ")
        var words = s.componentsSeparatedByCharactersInSet(separators)
        for i in 0...words.count-1 {
            words[i] = words[i] + " "
        }
        return words
    }
    func characterCount(s: String) -> Array<String> {
        let characters = s.characters.map { String($0) }
        return characters
    }
    func smallWordCounter(s: String) -> Int {
        let words = s.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        return words.count
    }
    @IBOutlet weak var startButton: UIButton!
    let texts:NSArray = ["-150wps- TOEIC500Lv.", "-250wps- TOEIC600Lv.", "-300wps- TOEIC750Lv.", "-350wps- TOEIC850Lv.", "-450wps- TOEIC990Lv.", "-550wps-", "-650wps-" ]
    var speedLevel:Int = 150
    var arrayCharacter = [String]()
    var countArray = [Int]()
    var tmpCounter:Double?
    var colour = [UIColor]()
    var myString = [NSMutableAttributedString]()
    var totalTimeCounter = [Double]()
    var tmpTotalTimeCounter = Int()
    var globalEnglishLabel = [NSMutableAttributedString]()
    var mainRow:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        pauseLabel.hidden = true
        englishLabel.hidden = false
        englishLabel.lineBreakMode = NSLineBreakMode.ByCharWrapping
        englishLabel.text = mainEnglishText
        //        showCountSecond.text = String(Int(counter))
        arrayCharacter = wordCount(englishLabel.text!)
        print(arrayCharacter)
        let wordNumber = arrayCharacter.count
        print(wordNumber)
        colour = [UIColor](count:arrayCharacter.count, repeatedValue:UIColor())
        myString = [NSMutableAttributedString](count:arrayCharacter.count, repeatedValue:NSMutableAttributedString())
        totalTimeCounter = [Double](count:arrayCharacter.count, repeatedValue:0)
        globalEnglishLabel = [NSMutableAttributedString](count:arrayCharacter.count, repeatedValue:NSMutableAttributedString())
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        for i in 0...arrayCharacter.count-1 {
            tmpCounter = 60/Double(speedLevel)
            if i == 0 {
                totalTimeCounter[i] = tmpCounter!
                for j in 0...arrayCharacter.count-1 {
                    if i == j {
                        colour[0] = UIColor.redColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[0] ]
                        myString[0] = NSMutableAttributedString(string: arrayCharacter[0], attributes: myAttribute)
                    } else {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[0] = myString[0]
            } else {
                totalTimeCounter[i] = totalTimeCounter[i-1] + tmpCounter!
                for j in 0...arrayCharacter.count-1 {
                    if j == 0 {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                    } else if i == j {
                        colour[j] = UIColor.redColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    } else {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[i] = myString[0]
            }
        }
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func onUpdate(){
        counter += 0.1
        //        showCountSecond.text = String(Int(counter))
        for i in 0...arrayCharacter.count-1 {
            if i == 0{
                if counter < totalTimeCounter[0] {
                    englishLabel.attributedText = globalEnglishLabel[0]
                }
            } else if i != 0 {
                if counter >= totalTimeCounter[i-1] && counter < totalTimeCounter[i]{
                    englishLabel.attributedText = globalEnglishLabel[i]
                    if i == arrayCharacter.count-2 {
                        timer.invalidate()
                        showCountSecond.text = "finish"
                        englishLabel.attributedText = globalEnglishLabel[0]
                        startButton.hidden = false
                        pauseLabel.hidden = true
                        counter = 0
                    }
                }
            }
        }
    }
    
    //表示列
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //表示個数
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return texts.count
    }
    
    //表示内容
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return texts[row] as! String
    }
    
    //選択時
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        speedLevel = row
        switch row {
        case 0 :
            speedLevel = 150
        case 1 :
            speedLevel = 250
        case 2 :
            speedLevel = 300
        case 3 :
            speedLevel = 350
        case 4 :
            speedLevel = 450
        case 5 :
            speedLevel = 550
        case 6 :
            speedLevel = 650
        default:
            speedLevel = 150
        }
        
        for i in 0...arrayCharacter.count-1 {
            tmpCounter = 60/Double(speedLevel)
            if i == 0 {
                totalTimeCounter[i] = tmpCounter!
                for j in 0...arrayCharacter.count-1 {
                    if i == j {
                        colour[0] = UIColor.redColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[0] ]
                        myString[0] = NSMutableAttributedString(string: arrayCharacter[0], attributes: myAttribute)
                    } else {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[0] = myString[0]
            } else {
                totalTimeCounter[i] = totalTimeCounter[i-1] + tmpCounter!
                for j in 0...arrayCharacter.count-1 {
                    if j == 0 {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                    } else if i == j {
                        colour[j] = UIColor.redColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    } else {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[i] = myString[0]
            }
        }
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
