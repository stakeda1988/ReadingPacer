//
//  ResultViewController.swift
//  StepReading
//
//  Created by SHOKI TAKEDA on 11/3/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, NADViewDelegate {
    private var nadView: NADView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var showCountSecond: UILabel!
    var timer:NSTimer!
    var startCount = true
    var counter:Double = 0
    var mainEnglishText:String?
    var adTimer:NSTimer!
    @IBOutlet weak var pauseLabel: UIButton!
    @IBAction func pauseButton(sender: UIButton) {
        timer.invalidate()
        startButton.hidden = false
        pauseLabel.hidden = true
    }
    @IBAction func startSentences(sender: UIButton) {
//        englishLabel.attributedText = globalEnglishLabel[0]
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
        startButton.hidden = true
        pauseLabel.hidden = false
        showCountSecond.text = ""
    }
    func wordCount(s: String) -> Array<String> {
        let separators = NSCharacterSet(charactersInString: " ")
        var words = s.componentsSeparatedByCharactersInSet(separators)
        for i in 0...words.count-1 {
            words[i] = words[i] + " "
        }
        return words
    }
    func characterCount(s: String) -> Array<String> {
        let characters = s.characters.map { String($0) }
        return characters
    }
    func smallWordCounter(s: String) -> Int {
        let words = s.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        return words.count
    }
    @IBOutlet weak var startButton: UIButton!
    let texts:NSArray = ["-150wps- TOEIC500Lv.", "-250wps- TOEIC600Lv.", "-300wps- TOEIC750Lv.", "-350wps- TOEIC850Lv.", "-450wps- TOEIC990Lv.", "-550wps-", "-650wps-" ]
    var speedLevel:Int = 150
    var arrayCharacter = [String]()
    var countArray = [Int]()
    var tmpCounter:Double?
    var colour = [UIColor]()
    var myString = [NSMutableAttributedString]()
    var totalTimeCounter = [Double]()
    var tmpTotalTimeCounter = Int()
    var globalEnglishLabel = [NSMutableAttributedString]()
    var mainRow:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        pauseLabel.hidden = true
        englishLabel.hidden = false
        englishLabel.lineBreakMode = NSLineBreakMode.ByCharWrapping
        englishLabel.text = mainEnglishText
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        
        switch mainRow {
            case 0:
                englishLabel.text = "Starting next month, we are going to begin phasing out our monthly catalog and move everything online. Given the current rise in paper costs coupled with the increase in shipping costs, it is no longer financially feasible to send out a catalog to our regular customers, nor to those who request one. Based on feedback from customers, we've learned that most of them prefer placing orders online because it is convenient and saves them time. Our plan is to put our entire catalog online along with a few special features that can't be included in our print version due to cost, space, etc. First, we will have more pictures for each product. Second, there will be able to download all instruction manuals at no cost through our online store. Whether or not to provide a print catalog for a small fee is still being debated. We will have to see if there is a demand before we make a decision on that. I'll be sending up follow-up e-mails regarding the development of this project in the upcoming weeks."
            case 1:
                englishLabel.text = "For the past 20 years, Satoh Goods has been the leading supplier of Japanese-made furniture and linens in the tai-state area. On behalf of everyone here at Kurata Goods, we wish to thank you for your continued patronage. Our store is celebrating its 20th Anniversary this month by offering our regular customers special discounts on our entire inventory. Choose from specially imported American beds and tables, leather French sofas and chairs, hight-quality Italian lamps, and much, much more! Customers who purchase an item from our antiques section will receive 20% off any additional piece of equal price or less. Sale lasts until the end of December, so don’t miss out. Customers who bring this postcard with them can use it to enter our drawing to win a Japanese Table with Mirror - a $4,300 value."
            case 2:
                englishLabel.text = "I know you’ve been quite busy with the Suzuki account, and with some of the recent staff shortages I realise your team has been working beyond its limits. I’m pleased to inform you that I’ll be transferring Yuta Satoh and Suzuki Taro to your team for the duration of your project. They just finished working on the Tanaka account the day before yesterday, so I’ll probably send them over either this Thursday or on Friday the 19th. They both have extensive experience in marketing ranging from small, private businesses to the large multinationals./n Another reason why I can send you Satoh and Suzuki is because on June 30 one summer intern and two new hires will begin working here. Once they finish up with general training, I’ll make a decision about where to send them. Since your team has been dealing with staff shortages, I was planing on sending one of the new hires and the intern to you. However, if you feel that you absolutely have no time to train them and that doing so would impede your ability to meet deadlines, I will send them to other teams. Please let me know before the end of this month."
            case 3:
                englishLabel.text = "As of February 10, all customers who purchased XAIO laptops(models XAIO-54S and XAIO-35C) are asked to exchange their purchases as soon as possible. XAIO has issued a recall order for these models owing to faulty hard drives. These hard drives have shown signs of overheating and a tendency to lose data. Recent user complaints have led XAIO to issue a recall for all laptops in the 54S and 35C model series. All consumers who have purchased XAIO laptops that are NOT the XAIO-54S or XAIO-35C models may have defective batteries that overheat. If you have a laptop with a XAIO laptop battery XAC-40V, please bring in your battery and exchange it for a replacement. You may choose from a Xenix-brand battery or a Xenix-approved third-party battery (Razor, GHUN, or Crimson batteries are all approved). Choosing the latter option will not void your laptop’s warranty unless you choose a non-approved third-party product. For more information, please go to www.xaio.com/products."
            case 4:
                englishLabel.text = "I'm applying for the position of Senior Project Manager for Troit Corporation as advertised in the October 30 issue of the IT Journal. As my attached résumé shows, I have an extensive background in banking, investing, and finance management. When I worked for Tokyo Solutions, I was in charge of multiple clients and advised them on appropriate investment paths including application development. I started out with handling small project and worked my way up to customer service for many blue-chip companies. During my time there, my team never failed. At Tokyo Consulting I managed investment portfolios for many companies. Although I worked independent of any team support, I did have to meet with and guide the representatives of various companies throughout the investment process. In my last job I worked as part of a team that does consulting for major government and private-sector firms. I took the lead on many projects and brought many new clients to the company. I am available for an interview at any time. I thank you in advance, and I look forward to hearing from you."
            
            case 5:
                englishLabel.text = "Kosuke Tanaka was employed as consultant at Molgan Consulting for two years. He left the company after many successes to further his education. During his tenure with Molgan Consulting, Mr.Tanaka worked on teams that handled consulting for various federal and state companies, and many private-sector firms. He was instrumental in landing many lucrative contracts for Molgan Consulting. Mr.Tanaka also showed exemplary leadership skills whenever he filled in the role of team leader. He is a great communicator, and she has unparalleled interpersonal skills. I recommend Mr.Tanaka for any management or supervisor position. Please call if you would like any specific details regarding his time here. Ask for Kenji Oda, his team leader."
            
            case 6:
                englishLabel.text = "Voison is going to unveil its entry into the portable gaming system market at October 1 Toyosu Electronics Expo. This highly anticipated device is expected to combine all the best features of a personal digital assistant with an extremely powerful portable gaming system. The 10 inch screen can display over 100 million colors and game graphics are smooth and fluid. For the PDA functions, there is a slide-out keyboard and touch-screen function, in addition to 20 gigs of internal memory. The built-in wi-fi connection will allow you to access the Internet or play your games online. On hand to Voison's UP will be company president Daisuke Matsumoto, as well Yuko Takahashi, head of marketing, and Yuki Matsushita, head of Voison's gaming department. Also on hand will be Akio Kuriyama of Major Games and creator of the wildly popular 'Up in Arms' series of games. He will be discussing some of Major's UP-exclusive titles."
            
            case 7:
                englishLabel.text = "'We already dominate the PDA and hand-held games market, so it was only natural for us to combine the two,' was the bold statement Voison President Daisuke Matsumoto chose to open the press conference to unveil Voison's PDA-cum-game machine. Can Voison create a new market with this device? That remains to be seen. The Ultra Portable would be an impressive PDA or gaming machine on its own, but the combination is somewhat underwhelming The fact is that most gaming devices are relatively simple, but PDAs are not. Historically, the target audiences for PDAs and games have been separate, even if the ages of the two overlap. Volson is aiming for the PDA user who also want to play games -a niche market to be sure. However, children and teens are a key gaming demographic. Can the UP survive merely by catering to the over-thirty crowd? This should be Voison's primary concern. While the specs for the device are impressive, Major Games is the only third party license they have now. Unfortunately, Voison's head of their gaming division was not at the trade show, so there is no word on whether UP will have any other company's game titles at launch."
            
            default:
                englishLabel.text = "Starting next month, we are going to begin phasing out our monthly catalog and move everything online. Given the current rise in paper costs coupled with the increase in shipping costs, it is no longer financially feasible to send out a catalog to our regular customers, nor to those who request one. Based on feedback from customers, we've learned that most of them prefer placing orders online because it is convenient and saves them time. Our plan is to put our entire catalog online along with a few special features that can't be included in our print version due to cost, space, etc. First, we will have more pictures for each product. Second, there will be able to download all instruction manuals at no cost through our online store. Whether or not to provide a print catalog for a small fee is still being debated. We will have to see if there is a demand before we make a decision on that. I'll be sending up follow-up e-mails regarding the development of this project in the upcoming weeks."
        }
//        showCountSecond.text = String(Int(counter))
        arrayCharacter = wordCount(englishLabel.text!)
        print(arrayCharacter)
        let wordNumber = arrayCharacter.count
        print(wordNumber)
        colour = [UIColor](count:arrayCharacter.count, repeatedValue:UIColor())
        myString = [NSMutableAttributedString](count:arrayCharacter.count, repeatedValue:NSMutableAttributedString())
        totalTimeCounter = [Double](count:arrayCharacter.count, repeatedValue:0)
        globalEnglishLabel = [NSMutableAttributedString](count:arrayCharacter.count, repeatedValue:NSMutableAttributedString())
        for i in 0...arrayCharacter.count-1 {
            tmpCounter = 60/Double(speedLevel)
            if i == 0 {
                totalTimeCounter[i] = tmpCounter!
                for j in 0...arrayCharacter.count-1 {
                    if i == j {
                        colour[0] = UIColor.redColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[0] ]
                        myString[0] = NSMutableAttributedString(string: arrayCharacter[0], attributes: myAttribute)
                    } else {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[0] = myString[0]
            } else {
                totalTimeCounter[i] = totalTimeCounter[i-1] + tmpCounter!
                for j in 0...arrayCharacter.count-1 {
                    if j == 0 {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                    } else if i == j {
                        colour[j] = UIColor.redColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    } else {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[i] = myString[0]
            }
        }
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func onUpdate(){
        counter += 0.1
//        showCountSecond.text = String(Int(counter))
        for i in 0...arrayCharacter.count-1 {
            if i == 0{
                if counter < totalTimeCounter[0] {
                    englishLabel.attributedText = globalEnglishLabel[0]
                }
            } else if i != 0 {
                if counter >= totalTimeCounter[i-1] && counter < totalTimeCounter[i]{
                    englishLabel.attributedText = globalEnglishLabel[i]
                    if i == arrayCharacter.count-2 {
                        timer.invalidate()
                        showCountSecond.text = "finish"
                        englishLabel.attributedText = globalEnglishLabel[0]
                        startButton.hidden = false
                        pauseLabel.hidden = true
                        counter = 0
                    }
                }
            }
        }
    }
    
    //表示列
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //表示個数
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return texts.count
    }
    
    //表示内容
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return texts[row] as! String
    }
    
    //選択時
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        speedLevel = row
        switch row {
        case 0 :
            speedLevel = 150
        case 1 :
            speedLevel = 250
        case 2 :
            speedLevel = 300
        case 3 :
            speedLevel = 350
        case 4 :
            speedLevel = 450
        case 5 :
            speedLevel = 550
        case 6 :
            speedLevel = 650
        default:
            speedLevel = 150
        }
        
        for i in 0...arrayCharacter.count-1 {
            tmpCounter = 60/Double(speedLevel)
            if i == 0 {
                totalTimeCounter[i] = tmpCounter!
                for j in 0...arrayCharacter.count-1 {
                    if i == j {
                        colour[0] = UIColor.redColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[0] ]
                        myString[0] = NSMutableAttributedString(string: arrayCharacter[0], attributes: myAttribute)
                    } else {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[0] = myString[0]
            } else {
                totalTimeCounter[i] = totalTimeCounter[i-1] + tmpCounter!
                for j in 0...arrayCharacter.count-1 {
                    if j == 0 {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                    } else if i == j {
                        colour[j] = UIColor.redColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    } else {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[i] = myString[0]
            }
        }
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
